const express = require('express');
const socketIO = require('socket.io');
const path = require('path');
const http = require('http');
const app = express();

const port = process.env.PORT || 3000;

// creando servidor
const server = http.createServer(app);

// dando acceso a la carpeta publica
const publicPath = path.resolve(__dirname, './public');
app.use(express.static(publicPath));

// comunicacion con el backend
module.exports.io = socketIO(server);
require('./sockets/socket');

server.listen(port, () => {
  console.log(`http://localhost:${port}`);
});
