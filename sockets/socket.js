const { io } = require('../index');
const { TicketControl } = require('../classes/ticket-control');

const ticketControl = new TicketControl();

// console.log(ticketControl);
io.on('connection', (client) => {
  // console.log('Usuario conectado');

  // referencia a nuevo ticket
  client.on('siguienteTicket', (data, callback) => {
    let siguientes = ticketControl.siguiente();
    console.log(`${siguientes}`);
    callback(siguientes);
  });

  client.emit('estadoActual', {
    actual: ticketControl.getUltimoTicket(),
    ultimos4: ticketControl.getUltimos4(),
  });

  // escritorio
  client.on('atenderTicket', (data, callback) => {
    if (!data.escritorio) {
      return callback({
        message: 'El escritorio es necesario',
      });
    }

    let atenderTicket = ticketControl.atenderTicket(data.escritorio);
    callback(atenderTicket);

    client.broadcast.emit('ultimos4', {
      ultimos4: ticketControl.getUltimos4(),
    });
  });
});
