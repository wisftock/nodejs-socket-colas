// comando para establecer la conexion
const h1 = document.querySelector('h1');
const small = document.querySelector('small');
const boton = document.querySelector('.boton');
const socket = io();

const serachParams = new URLSearchParams(window.location.search);
if (!serachParams.has('escritorio')) {
  window.location = 'index.html';
  throw new Error('El escritorio es necesario');
}

const escritorio = serachParams.get('escritorio');
h1.innerHTML = `Escritorio : ${escritorio}`;

boton.addEventListener('click', function () {
  socket.emit('atenderTicket', { escritorio }, function (resp) {
    console.log(resp);
    if (resp === 'No hay tickets') {
      small.innerHTML = resp;
      alert(resp);
      return;
    }
    small.innerHTML = resp.numero;
  });
});
