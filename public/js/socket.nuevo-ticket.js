// comando para establecer la conexion
const label = document.getElementById('lblNuevoTicket');
const socket = io();
socket.on('connect', (data) => {
  console.log('conectado');
});

socket.on('disconnect', (data) => {
  console.log('desconectado');
});

socket.on('estadoActual', function (respt) {
  label.textContent = respt.actual;
  console.log(respt);
});

const nuevoTicket = document.querySelector('.nuevoTickets');
nuevoTicket.addEventListener('click', () => {
  //   console.log('click');
  socket.emit('siguienteTicket', null, function (siguienteTicket) {
    label.textContent = `${siguienteTicket}`;
  });
});

// console.log(label);
